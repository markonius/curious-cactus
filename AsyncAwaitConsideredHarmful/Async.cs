using System;
using System.Linq;
using System.Threading.Tasks;

class Async {
	static Task SendOverInternet(string address, string data) => Task.FromResult(0);
	static Task Print(string text) => Console.Out.WriteLineAsync(text);
	static Task<int> Length(string str) => Task.FromResult(str.Length);
	static Task<bool> Any(string str, Func<char, bool> predicate) => Task.Run(() => str.Any(predicate));

	static async Task Main() {
		string password = await GetPasswordFromUser();
		ReportToNSA(password);
		int score = await RatePassword(password);

		Print($"Your password's score is {score}/5.");
	}

	static async Task<string> GetPasswordFromUser() {
		await Print("Please enter your password.");
		return Console.ReadLine();
	}

	static async Task ReportToNSA(string password) {
		await SendOverInternet("www.nsa.gov", password);
	}

	static async Task<int> RatePassword(string password) {
		Task<int> lengthScore = RateLength(password);
		Task<int> varietyScore = RateVariety(password);
		return (await lengthScore + await varietyScore) / 2;
	}

	static async Task<int> RateLength(string password) {
		int lengthScore = await Length(password) / 4;
		return Math.Min(lengthScore, 5);
	}

	static async Task<int> RateVariety(string password) {
		int score = 0;

		Task<bool> hasLowercase = Any(password, char.IsLower);
		Task<bool> hasUppercase = Any(password, char.IsUpper);
		Task<bool> hasNumber = Any(password, char.IsNumber);
		Task<bool> hasSymbol = Any(password, char.IsSymbol);
		Task<bool> hasUnicode = Any(password, c => c > 255);

		if (await hasLowercase) score++;
		if (await hasUppercase) score++;
		if (await hasNumber) score++;
		if (await hasSymbol) score++;
		if (await hasUnicode) score++;
		return score;
	}

}
