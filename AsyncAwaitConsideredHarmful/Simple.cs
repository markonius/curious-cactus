﻿using System;
using System.Linq;

class Simple {
	// Sends some data over the internet
	static void SendOverInternet(string address, string data) { }
	// Prints text to standard output
	static void Print(string text) => Console.WriteLine(text);
	// Returns the length of a string
	static int Length(string str) => str.Length;
	// Returns true if predicate is true for any character in a string
	static bool Any(string str, Func<char, bool> predicate) => str.Any(predicate);

	static void Main() {
		string password = GetPasswordFromUser();
		ReportToNSA(password);
		int score = RatePassword(password);

		Print($"Your password's score is {score}/5.");
	}

	static string GetPasswordFromUser() {
		Print("Please enter your password.");
		return Console.ReadLine();
	}

	static void ReportToNSA(string password) {
		SendOverInternet("www.nsa.gov", password);
	}

	static int RatePassword(string password) {
		int lengthScore = RateLength(password);
		int varietyScore = RateVariety(password);
		return (lengthScore + varietyScore) / 2;
	}

	static int RateLength(string password) {
		int lengthScore = Length(password) / 4;
		return Math.Min(lengthScore, 5);
	}

	static int RateVariety(string password) {
		int score = 0;

		bool hasLowercase = Any(password, char.IsLower);
		bool hasUppercase = Any(password, char.IsUpper);
		bool hasNumber = Any(password, char.IsNumber);
		bool hasSymbol = Any(password, char.IsSymbol);
		bool hasUnicode = Any(password, c => c > 255);

		if (hasLowercase) score++;
		if (hasUppercase) score++;
		if (hasNumber) score++;
		if (hasSymbol) score++;
		if (hasUnicode) score++;
		return score;
	}

}
