using System.Linq;
using System.Threading.Tasks;

class AsyncProblemPrelude {
	static int Add(int a, int b) => a + b;
	static int AddTwo(int n) => Add(n, 2);

	static void Main() {
		int number = 5;
		int largerNumber = AddTwo(number);
		int negativeNumber = -largerNumber;

		int[] numbers = new[] { 1, 2, 3, 4, 5 };
		int[] largerNumbers = numbers.Select(AddTwo).ToArray();
	}
}

class AsyncProblem {
	static Task<int> Add(int a, int b) => Task.FromResult(a + b);
	static async Task<int> AddTwo(int n) => await Add(n, 2);

	static async Task Main() {
		int number = 5;
		int largerNumber = await AddTwo(number);
		int negativeNumber = -largerNumber;

		int[] numbers = new[] { 1, 2, 3, 4, 5 };
		int[] largerNumbers = numbers.Select(n => AddTwo(n).Result).ToArray();
	}
}