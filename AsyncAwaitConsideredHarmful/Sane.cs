using System;
using System.Linq;
using System.Threading.Tasks;

class Sane {
	static void SendOverInternet(string address, string data) { }
	static void Print(string text) => Console.WriteLine(text);
	static int Length(string str) => str.Length;
	static bool Any(string str, Func<char, bool> predicate) => str.Any(predicate);

	static void Main() {
		string password = GetPasswordFromUser();
		Task.Run(() => ReportToNSA(password));
		int score = RatePassword(password);

		Print($"Your password's score is {score}/5.");
	}

	static string GetPasswordFromUser() {
		Print("Please enter your password.");
		return Console.ReadLine();
	}

	static void ReportToNSA(string password) {
		SendOverInternet("www.nsa.gov", password);
	}

	static int RatePassword(string password) {
		Task<int> lengthScore = Task.Run(() => RateLength(password));
		Task<int> varietyScore = Task.Run(() => RateVariety(password));
		return (lengthScore.Result + varietyScore.Result) / 2;
	}

	static int RateLength(string password) {
		int lengthScore = Length(password) / 4;
		return Math.Min(lengthScore, 5);
	}

	static int RateVariety(string password) {
		int score = 0;

		Task<bool> hasLowercase = Task.Run(() => Any(password, char.IsLower));
		Task<bool> hasUppercase = Task.Run(() => Any(password, char.IsUpper));
		Task<bool> hasNumber = Task.Run(() => Any(password, char.IsNumber));
		Task<bool> hasSymbol = Task.Run(() => Any(password, char.IsSymbol));
		Task<bool> hasUnicode = Task.Run(() => Any(password, c => c > 255));

		if (hasLowercase.Result) score++;
		if (hasUppercase.Result) score++;
		if (hasNumber.Result) score++;
		if (hasSymbol.Result) score++;
		if (hasUnicode.Result) score++;
		return score;
	}

	static Task<bool> AsyncAny(string str, Func<char, bool> predicate) => Task.Run(() => Any(str, predicate));
	static void Addendum(string password) {
		Task<bool> hasLowercase = AsyncAny(password, char.IsLower);
		Task<bool> hasUppercase = AsyncAny(password, char.IsUpper);
		Task<bool> hasNumber = AsyncAny(password, char.IsNumber);
		Task<bool> hasSymbol = AsyncAny(password, char.IsSymbol);
		Task<bool> hasUnicode = AsyncAny(password, c => c > 255);
	}
}
